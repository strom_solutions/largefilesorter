
# README #


### Innehåll ###

.NET Standard klassbibliotek LargeFileSort för att sotera filer i miljöer med begränsat minne enl. uppgift:

Skriv ett program som sorterar en textfil i alfanumerisk ordning med följande förutsättningar
- Tillgängligt RAM-minne är begränsat
- Utfiler:
	- Varje ny menig i in-filen skall resultera i en ny rad i ut-filen
	- Varje ut-fil får vara max 1MB stor. Första ut-filen döps till 1.txt andra till 2.txt ect.


Programflödet i korthet:

Klassen initialiseras med parametrar enligt instruktioner nedan.

Det öppnas en läsström som läser in-filen rad för rad.

Raden kapas upp efter angivna avgränsare, och meningarna skrivs till en temporärfil med max angiven storlek.

Efter detta öppnas temporärfilerna upp och sorteras. 

Efter detta ska filerna mergas ihop till en eller flera nya utdata filer enligt angiven storlek.

Detta görs genom att en läsström öppnas mot varje temporärfil.

Hela tiden plockas det lägsta värdet ut och skrivs rad för rad till utfil(-er) tills alla rader är skrivna. 


### Hur använder jag klassen ###

Klassen, LargeFileSorter, skapas med hjälp av följande konsturktorvariabler:
- string workingDir: Mappen där programmet arbetar, dvs. skapar temporära arbetsfiler och utfiler.
-  int outFileSize: Maximal storlek i KB för utfiler (0 = ingen gräns)
- int chunkSize: Maximal storlek för temporära filer i KB (0 = ingen gräns)

Exempel:
`LargeFileSorter sorter = new LargeFileSorter(@"d:\somepath", 1024, 1024);`

Dessutom kan följande egenskaper sättas på objektet:
- string[] Deimiters: Vilka avgränsare programmet ska använda för att bryta rader in i meningar.

T.ex nedan för att dela på semikolonseparerad textfil:

`sorter.Delimiters = new string[] { ";" };`

För att dela en fil använder man sen metoden SortByFile och anger sökvägen. Exempel: 
`sorter.SortByFile(@"d:\someotherpath\somefile.txt");`



När funktionen är klar ska sorterade utfiler ha skapats


### IN-filer ###

Programmet förutsätter att varje mening som ska sorteras inte är radbruten. 
Programmet läser in-filen rad för rad och delar upp efter angivna avgränasre (Delimiters) och skriver till temporära filer. 


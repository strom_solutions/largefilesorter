﻿using System;
using System.IO;
using System.Text;

namespace spm.largefilesorter
{
    /// <summary>
    /// Sorts a file in cunks and produces new output file or output files.
    /// Can be used where file size is bigger than available memory
    /// </summary>
    public class LargeFileSorter
    {
        /************************************************************************************************************************
         *  PRIVATE PROPERTIES
         ************************************************************************************************************************/
        private int _chunkSizeInBytes
        {
            get
            {
                return ChunkSize * 1024;
            }
        }

        private int _outFileSizeInBytes
        {
            get
            {
                return OutFileSize * 1024;
            }
        }

        private int _newLineByteCount;

        private string _tempDir;
        private string _outDir;

        /************************************************************************************************************************
         *  PUBLIC PROPERTIES
         ************************************************************************************************************************/

        public int OutFileSize { get; internal set; }
        public int ChunkSize { get; internal set; }
        public string WorkingDir { get; internal set; }

        public string[] Delimiters = { "." };

        /************************************************************************************************************************
         *  CONSTRUCTORS
         ************************************************************************************************************************/

        public LargeFileSorter(string workingDir, int outFileSize, int chunkSize)
        {
            //Initialize all properties
            OutFileSize = outFileSize;
            ChunkSize = chunkSize;
            WorkingDir = workingDir;

            _newLineByteCount = Encoding.Default.GetByteCount(Environment.NewLine);

            //Create working directory
            _tempDir = Path.Combine(workingDir, "tmp");
            _outDir = Path.Combine(workingDir, "out");
            Directory.CreateDirectory(_tempDir);
            Directory.CreateDirectory(_outDir);

        }

        /************************************************************************************************************************
         *  PUBLIC METHODS
         ************************************************************************************************************************/

        /// <summary>
        ///  Performs sorting based on file on given path. 
        /// </summary>
        public void SortByFile(string filePath)
        {
            try
            {
                SplitFileIntoTempFiles(filePath);
                SortTempFiles();
                MergeFiles();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                Clean();
            }
            
        }

        /************************************************************************************************************************
         *  PRIVATE METHODS
         ************************************************************************************************************************/

        /// <summary>
        /// Cleans the tempFiles
        /// </summary>
        private void Clean()
        {
            string[] tempFiles = Directory.GetFiles(_tempDir);
            foreach(string tempFile in tempFiles)
            {
                File.Delete(tempFile);
            }

            Directory.Delete(_tempDir);
        }



        /************************************************************************************************************************
         *  SPLIT FUNCTIONALITY
         *      These methods togheter takes a large file and reads it by the row.
         *      The row is splitted and written to temporary files.
         *      When the maximum chunk size is reched a new file is created
         *      These files will need to be sorted before the final merge
         ************************************************************************************************************************/
        
        /// <summary>
        /// Split functionality
        /// </summary>
        private void SplitFileIntoTempFiles(string filePath)
        {
            StreamWriter writer = null;
            try
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    int currentTempFile = 1;

                    string tmpFilePath = Path.Combine(_tempDir, $"{currentTempFile}.txt");
                    writer = new StreamWriter(tmpFilePath);

                    //Read until all file data is processed
                    while (reader.Peek() > 0)
                    {
                        string readRow = reader.ReadLine();
                        string[] rows = readRow.Split(Delimiters, StringSplitOptions.RemoveEmptyEntries);
                        readRow = null;

                        WriteLinesToTempFile(rows, ref writer, ref currentTempFile);
                    }

                }
            }
            catch
            {
                throw;
            }
            finally
            {
                writer.Close();
            }

            
        }

        private void WriteLinesToTempFile(string[] rows, ref StreamWriter writer, ref int currentTempFile)
        {
            for (int i = 0; i < rows.Length; i++)
            {
                //If the data is empty continue to next iteration
                if(string.IsNullOrEmpty(rows[i]))
                {
                    continue;
                }

                WriteToLimitedFile(ref writer, rows[i], _chunkSizeInBytes, ref currentTempFile, _tempDir);
            }
        }

        /************************************************************************************************************************
         *  SORT FUNCTIONALITY
         *      These methods toghether sorts the chunked files created in the split functionaity.
         *      When each of the files contents is sorted
         ************************************************************************************************************************/

        private void SortTempFiles()
        {
            string[] tempFiles = Directory.GetFiles(_tempDir);

            foreach (string tempFile in tempFiles)
            {
                string[] rows = File.ReadAllLines(tempFile);

                Array.Sort(rows);

                //Delete the old file (just in case...)
                File.Delete(tempFile);
                File.WriteAllLines(tempFile, rows);

                rows = null;
            }
        }


        /************************************************************************************************************************
         *  MERGE FUNCTIONALITY
         *      Thes functions togheter will open one reader for each sorted temp file.
         *      Then select the lowest value from the readers and append to new files.
         *      If the max output size is reached it will start producing next output file eg. 1.txt, 2.txt etc.
         *      If the max output si
         ************************************************************************************************************************/
        private void MergeFiles()
        {
            StreamReader[] readers = null;
            try
            {
                string[] chunkFilePaths = Directory.GetFiles(_tempDir);
                int chunkFiles = chunkFilePaths.Length;

                string[] rows = new string[chunkFiles];

                readers = CreateStreamReaders(chunkFilePaths);
                for (int i = 0; i < chunkFiles; i++)
                {
                    rows[i] = GetNextRowFromReader(readers[i]);
                }

                MergeSortChunksReaders(readers, rows);

                for (int i = 0; i < chunkFiles; i++)
                {
                    readers[i].Close();
                    File.Delete(chunkFilePaths[i]);
                }

                readers = null;
            }
            catch
            {
                throw;
            }
            finally
            {
                if(readers != null)
                {
                    foreach(StreamReader reader in readers)
                    {
                        reader.Close();
                    }
                }
            }
            
        }

        /// <summary>
        /// Performs the sorting and saving of new files.
        /// Picks the lowest value from each reader and  
        /// </summary>
        private void MergeSortChunksReaders(StreamReader[] readers, string[] rows)
        {
            StreamWriter writer = null;
            try
            {
                int currentFile = 1;
                writer = new StreamWriter(Path.Combine(_outDir, $"{currentFile}.txt"), false, Encoding.Default);

                while (true)
                {
                    int pickIndex = -1;
                    string pickValue = "";

                    pickIndex = GetIndexForLowestString(rows);

                    if (pickIndex == -1)
                    {
                        break;
                    }

                    pickValue = rows[pickIndex];

                    WriteToLimitedFile(ref writer, pickValue, _outFileSizeInBytes, ref currentFile, _outDir);

                    rows[pickIndex] = GetNextRowFromReader(readers[pickIndex]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                writer.Close();
            }


            
        }


        private StreamReader[] CreateStreamReaders(string[] filePaths)
        {
            StreamReader[] readers = new StreamReader[filePaths.Length];
            for (int i = 0; i < readers.Length; i++)
            {
                readers[i] = new StreamReader(filePaths[i]);
            }

            return readers;
        }

        private int GetIndexForLowestString(string[] strings)
        {
            int pickIndex = -1;
            string pickValue = "";
            for (int i = 0; i < strings.Length; i++)
            {
                if (!string.IsNullOrEmpty(strings[i]))
                {
                    if (pickIndex == -1)
                    {
                        pickIndex = i;
                        pickValue = strings[i];
                    }
                    else
                    {
                        int stringComparision = String.CompareOrdinal(strings[i], pickValue);
                        if (stringComparision < 0)
                        {
                            pickIndex = i;
                            pickValue = strings[i];
                        }
                    }
                }
            }

            return pickIndex;
        }


        private string GetNextRowFromReader(StreamReader reader)
        {
            if (reader.Peek() > 0)
            {
                string line = reader.ReadLine();
                if(string.IsNullOrEmpty(line))
                {
                    GetNextRowFromReader(reader);
                }
                else
                {
                    return line;
                }
            }
            return null;
        }

        /************************************************************************************************************************
         * COMMON METHODS
         ************************************************************************************************************************/

        /// <summary>
        /// Writes string to path using writer.WriteLine. 
        /// Will increment fileindex and create new writer if nessecary (hence the refs)
        /// </summary>
        private void WriteToLimitedFile(ref StreamWriter writer, string value, int fileSizeLimit, ref int currentFileIndex, string directoryPath)
        {
            // Check if file writer should close and new one open
            // If the limit is 0 there is no limit
            // Else check the limit
            bool shouldWriterCloseAndOpenNew = fileSizeLimit != 0 && (writer.BaseStream.Length + Encoding.Default.GetByteCount(value) + _newLineByteCount) > fileSizeLimit;

            if (shouldWriterCloseAndOpenNew)
            {
                writer.Close();
                currentFileIndex++;
                string tmpFilePath = Path.Combine(directoryPath, $"{currentFileIndex}.txt");
                writer = new StreamWriter(tmpFilePath);
            }


            writer.WriteLine(value);
        }
    }



}

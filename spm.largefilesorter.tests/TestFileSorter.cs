using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace spm.largefilesorter.tests
{
    [TestClass]
    public class TestFileSorter
    {
        [TestMethod]
        public void TestFileSortByFilePath()
        {
            int chunkSizeInKb = 2048;
            int outSizeInKb = 0;
            string testFilePath = @"d:\temp\spmfile\text.txt";
            string outDir = @"d:\temp\spmfile\workingdir\";

            LargeFileSorter largeFileSorter = new LargeFileSorter(outDir, outSizeInKb, chunkSizeInKb);
            largeFileSorter.Delimiters = new string[] { ". ", "? ", "! " };
            largeFileSorter.SortByFile(testFilePath);
        }

        [TestMethod]
        public void TestSimpleFileSort()
        {
            int chunkSizeInKb = 2048;
            int outSizeInKb = 1024;
            string testFilePath = @"d:\temp\spmfile\simple.txt";
            string outDir = @"d:\temp\spmfile\workingdir\";

            LargeFileSorter largeFileSorter = new LargeFileSorter(outDir, outSizeInKb, chunkSizeInKb);
            largeFileSorter.Delimiters = new string[] { ";" };
            largeFileSorter.SortByFile(testFilePath);
        }

        [TestMethod]
        public void TestSmall()
        {
            int chunkSizeInKb = 10480;
            int outSizeInKb = 0;
            string testFilePath = @"d:\temp\spmfile\small.txt";
            string outDir = @"d:\temp\spmfile\smalldir\";

            LargeFileSorter largeFileSorter = new LargeFileSorter(outDir, outSizeInKb, chunkSizeInKb);
            largeFileSorter.Delimiters = new string[] { ";" };
            largeFileSorter.SortByFile(testFilePath);

        }
    }
}

﻿using System;
using spm.largefilesorter;

namespace spm.fileparseandsort
{
    class Program
    {
        private static string filePath;
        private static string outDir;
        private static int fileOutSizeInKb;
        private static int chunkSizeInKb;

        static void Main(string[] args)
        {
            Console.WriteLine("--- Press any key to start ---");
            Console.ReadLine();
            Console.WriteLine("--- Program started ---");

            bool validParameters = SetParameters(args);

            if (validParameters)
            {
                var sorter = new LargeFileSorter(outDir, fileOutSizeInKb, chunkSizeInKb);
                sorter.Delimiters = new string[] { ";" };
                try
                {
                    
                }
                catch(Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }

                sorter.SortByFile(filePath);
            }

            Console.WriteLine("--- Program ended ---");
            Console.ReadLine();
        }

        private static bool SetParameters(string[] args)
        {
            if (args.Length < 4)
            {
                Console.WriteLine("Error: Not enough arguments are passed to application");
                return false;
            }

            filePath = args[0];
            outDir = args[1];

            if(!int.TryParse(args[2], out fileOutSizeInKb))
            {
                Console.WriteLine("Error: File out size is not a number");
                return false;
            }

            if(!int.TryParse(args[3], out chunkSizeInKb))
            {
                Console.WriteLine("Error: Chunk size is not a number");
                return false;
            }

            return true;
        }


    }
}
